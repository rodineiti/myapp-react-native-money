/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 *
 * rodar primeiro o comando: npx react-native start
 * depois: npx react-native run-android
 */
import 'react-native-gesture-handler';
import React from 'react';
import MainStack from './navigations/MainStack';

export default function App() {
  return <MainStack />;
}
