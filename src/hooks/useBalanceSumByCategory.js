import {useState, useEffect} from 'react';

import {getBalanceSumByCategory} from './../services/Balance';

const useBalanceSumByCategory = (days = 7) => {
  const [balanceSum, setBalanceSum] = useState([]);

  useEffect(() => {
    const loadData = async () => {
      const data = await getBalanceSumByCategory(days);
      setBalanceSum([...data]);
    };
    loadData();
  }, [days]);

  return [balanceSum];
};

export default useBalanceSumByCategory;
