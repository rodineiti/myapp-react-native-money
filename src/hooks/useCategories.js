import {useState, useEffect} from 'react';
import {
  getInitCategories,
  getAllCategories,
  getDebitCategories,
  getCreditCategories,
} from '../services/Categories';

const useCategories = () => {
  const [allCategories, setAllCategories] = useState([]);
  const [initCategories, setInitCategories] = useState([]);
  const [debitCategories, setDebitCategories] = useState([]);
  const [creditCategories, setCreditCategories] = useState([]);

  useEffect(() => {
    const loadAllCategorias = async () => {
      setAllCategories(await getAllCategories());
    };
    const loadInitCategorias = async () => {
      setInitCategories(await getInitCategories());
    };
    const loadIDebitCategorias = async () => {
      setDebitCategories(await getDebitCategories());
    };
    const loadCreditCategorias = async () => {
      setCreditCategories(await getCreditCategories());
    };
    loadAllCategorias();
    loadInitCategorias();
    loadIDebitCategorias();
    loadCreditCategorias();
  }, []);

  return [allCategories, initCategories, debitCategories, creditCategories];
};

export default useCategories;
