import {useState, useEffect} from 'react';
import {getEntries, saveEntry, removeEntry} from '../services/Entries';

const useEntries = (days, category) => {
  const [entries, setEntries] = useState([]);

  useEffect(() => {
    const loadList = async () => {
      const data = await getEntries(days, category);
      setEntries(data);
    };
    loadList();
  }, [days, category]);

  return [entries, saveEntry, removeEntry];
};

export default useEntries;
