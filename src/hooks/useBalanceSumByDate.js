import {useState, useEffect} from 'react';

import {getBalanceSumByDate} from './../services/Balance';

const useBalanceSumByDate = (days = 7) => {
  const [balanceSum, setBalanceSum] = useState([]);

  useEffect(() => {
    const loadData = async () => {
      const data = await getBalanceSumByDate(days);
      setBalanceSum([...data]);
    };
    loadData();
  }, [days]);

  return [balanceSum];
};

export default useBalanceSumByDate;
