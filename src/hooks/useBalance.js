import {useState, useEffect} from 'react';

import {getBalance} from './../services/Balance';

const useBalance = () => {
  const [balance, setBalance] = useState(null);

  useEffect(() => {
    const loadBalance = async () => {
      const data = await getBalance();
      setBalance(data);
    };
    loadBalance();
  }, []);

  return [balance];
};

export default useBalance;
