import {Alert} from 'react-native';
import {getRealm} from './Realm';
import {getUUID} from './UUID';
import moment from './../vendors/moment';

export const saveEntry = async (value, entry = {}) => {
  const realm = await getRealm();
  let data = {};
  try {
    realm.write(() => {
      data = {
        id: value.id || entry.id || getUUID(),
        amount: value.amount || entry.amount || 0,
        description:
          value.description || entry.description || value.category.name,
        entryAt: value.entryAt || entry.entryAt || new Date(),
        isInit: value.isInit || false,
        category: value.category || entry.category,
        latitude: value.latitude || entry.latitude,
        longitude: value.longitude || entry.longitude,
        address: value.address || entry.address,
        photo: value.photo || entry.photo,
      };
      realm.create('Entry', data, true);
    });
    console.log('saveEntry :: success save object :' + JSON.stringify(data));
  } catch (error) {
    console.error('saveEntry :: error on save object :' + JSON.stringify(data));
    console.error('saveEntry :: error on save object ERROR :' + error);
    Alert.alert('', 'Ocorreu um erro ao salvar os dados de lançamento.');
  }

  return data;
};

export const getEntries = async (days, category) => {
  let realm = await getRealm();
  realm = realm.objects('Entry');

  if (days > 0) {
    const date = moment().subtract(days, 'days').toDate();
    realm = realm.filtered('entryAt >= $0', date);
  }

  if (category && category.id) {
    realm = realm.filtered('category == $0', category);
  }

  const data = realm.sorted('entryAt', true);

  console.log('saveEntry :: success get all object :' + data);

  return data;
};

export const getEntry = async (id) => {
  const realm = await getRealm();

  const data = realm.objects('Entry').filtered('id == $0', id);

  return data[0];
};

export const removeEntry = async (entry) => {
  const realm = await getRealm();

  try {
    realm.write(() => {
      realm.delete(entry);
    });
  } catch (error) {
    console.error(
      'removeEntry :: error on remove object :' + JSON.stringify(entry),
    );
    Alert.alert('', 'Ocorreu um erro ao deletar o lançamento.');
  }
};
