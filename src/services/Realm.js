import Realm from 'realm';

import CategorySchema from './../schemas/CategorySchema';
import EntrySchema from './../schemas/EntrySchema';
import {getDefaultCategories} from './Categories';

export const getRealm = async () => {
  const realm = await Realm.open({
    schema: [CategorySchema, EntrySchema],
    schemaVersion: 2,
  });

  //dropDB(realm);
  initDB(realm);

  return realm;
};

export const initDB = (realm) => {
  const categoriesLength = realm.objects('Category').length;
  console.log(`initDB => categories length: ${categoriesLength}`);

  if (categoriesLength === 0) {
    const categories = getDefaultCategories();

    console.log('initDB => initing db...');

    try {
      realm.write(() => {
        categories.forEach((item) => {
          console.log(`initDB => creating category: ${JSON.stringify(item)}`);

          realm.create('Category', item, true);
        });
      });
    } catch (error) {
      console.log('initDB => error: ' + error);
    }
  } else {
    console.log('initDB => categories already existing ... Skypping');
  }
};

export const dropDB = (realm) => {
  console.log('dropDB => dropping db...');
  realm.write(() => {
    realm.deleteAll();
  });
};
