export const getUUID = () => {
  const time = new Date().getTime();
  const random = Math.floor(Math.random() * 999999999);
  return `${time}-${random}`;
};
