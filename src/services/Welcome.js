import AsyncStorage from '@react-native-community/async-storage';

export const getConfig = async () => {
  const isConfig = await AsyncStorage.getItem('isConfig');

  return isConfig !== null && isConfig === 'true';
};

export const setConfig = async () => {
  await AsyncStorage.setItem('isConfig', 'true');
};

export const resetConfig = async () => {
  await AsyncStorage.removeItem('isConfig');
};
