const {getRealm} = require('../services/Realm');
import _ from 'lodash';
import Colors from '../styles/Colors';
import {getUUID} from './../services/UUID';
import moment from './../vendors/moment';

export const getBalance = async (untilDays = 0) => {
  const realm = await getRealm();
  let entries = realm.objects('Entry');

  if (untilDays > 0) {
    const date = moment().subtract(untilDays, 'days').toDate();
    entries = entries.filtered('entryAt < $0', date);
  }

  return entries.sum('amount');
};

export const getBalanceSumByDate = async (days) => {
  const realm = await getRealm();

  const startBalance = await getBalance(days);

  let entries = realm.objects('Entry');

  if (days > 0) {
    const date = moment().subtract(days, 'days').toDate();
    entries = entries.filtered('entryAt >= $0', date);
  }

  entries = entries.sorted('entryAt');

  entries = _(entries)
    .groupBy(({entryAt}) => moment(entryAt).format('YYYYMMDD'))
    .map((item) => _.sumBy(item, 'amount'))
    .map((amount, index, collection) => {
      return (
        (index === 0 ? startBalance : 0) +
        _.sum(_.slice(collection, 0, index)) +
        amount
      );
    });

  console.log('getBalanceSumByDate => ', JSON.stringify(entries));

  return entries;
};

export const getBalanceSumByCategory = async (days, showOthers = true) => {
  const realm = await getRealm();

  let entries = realm.objects('Entry');

  if (days > 0) {
    const date = moment().subtract(days, 'days').toDate();
    entries = entries.filtered('entryAt >= $0', date);
  }

  entries = entries.sorted('entryAt');

  entries = _(entries)
    .groupBy(({category: {id}}) => id)
    .map((item) => ({
      category: _.omit(item[0].category, 'entries'),
      amount: Math.abs(_.sumBy(item, 'amount')),
    }))
    .filter(({amount}) => amount > 0)
    .orderBy('amount', 'desc');

  const limitOthers = 3;

  if (showOthers && _(entries).size() > limitOthers) {
    const data1 = _(entries).slice(0, limitOthers);
    const data2 = [
      {
        category: {
          id: getUUID(),
          name: 'Outros',
          color: Colors.metal,
          amount: _(entries)
            .slice(limitOthers)
            .map(({amount}) => amount)
            .sum(),
        },
      },
    ];

    entries = [...data1, ...data2];
  }

  console.log('getBalanceSumByCategory => ', JSON.stringify(entries));

  return entries;
};
