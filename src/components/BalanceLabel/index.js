import React from 'react';
import styled from 'styled-components/native';
import LinearGradient from 'react-native-linear-gradient';
import useBalance from './../../hooks/useBalance';

import Colors from '../../styles/Colors';
import Currency from '../Currency';

const View = styled.View`
  align-items: center;
  padding: 20px;
`;

const Label = styled.Text`
  font-size: ${(props) => props.sizeLabel}px;
  color: ${(props) => props.color || '#000'};
`;

const Value = styled.Text`
  font-size: ${(props) => props.sizeValue}px;
  color: ${(props) => props.color || '#000'};
`;

const Linear = styled(LinearGradient)`
  padding-top: 10px;
  padding-bottom: 10px;
  padding-right: 15px;
  padding-left: 15px;
  margin-top: 10px;
  border-radius: 10px;
`;

export default function BalanceLabel({sizeLabel = 14, sizeValue = 28}) {
  const [balance] = useBalance();
  return (
    <View>
      <Label color={Colors.white} sizeLabel={sizeLabel}>
        Saldo atual
      </Label>
      <Linear colors={[Colors.violet, Colors.blue]}>
        <Value color={Colors.white} sizeValue={sizeValue}>
          <Currency value={balance} />
        </Value>
      </Linear>
    </View>
  );
}
