import React from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Svg, {Circle, Rect} from 'react-native-svg';
import moment from './../../../vendors/moment';
import Colors from '../../../styles/Colors';
import Currency from '../../Currency';

const Wrapper = styled.View`
  flex: 1;
  flex-direction: row;
`;

const ViewBullet = styled.View``;

const View = styled.View`
  flex: 1;
  justify-content: center;
`;

const Button = styled.TouchableOpacity``;

const Description = styled.Text`
  font-size: 14px;
  color: ${(props) => props.color || '#fff'};
`;

const Details = styled.View`
  flex-direction: row;
`;

const EntryAt = styled.Text`
  font-size: 12px;
  color: ${(props) => props.color || '#fff'};
`;

const Address = styled.Text`
  font-size: 12px;
  color: ${(props) => props.color || '#fff'};
`;

const ViewAmount = styled.View`
  justify-content: center;
`;

const Amount = styled.Text`
  font-size: 14px;
  font-weight: bold;
  color: ${(props) => props.color || '#fff'};
`;

export default function EntryListItem({item, onPress, isFirst, isLast}) {
  const bulletLineY = isFirst ? 25 : 0;
  const bulletLineHeight = isLast ? 25 : 50;
  const bulletLineLine = !(isFirst && isLast);
  const bulletColor = item.category.color || Colors.white;

  return (
    <Button
      onPress={() => {
        onPress && onPress(item);
      }}>
      <Wrapper>
        <ViewBullet>
          <Svg height="50" width="30">
            {bulletLineLine && (
              <Rect
                x="9"
                y={bulletLineY}
                width="1.5"
                height={bulletLineHeight}
                fill={Colors.background}
              />
            )}

            <Circle
              cx="10"
              cy="25"
              r={8}
              stroke={Colors.background}
              strokeWidth="1.5"
              fill={bulletColor}
            />
          </Svg>
        </ViewBullet>
        <View>
          <Description color={Colors.white}>{item.description}</Description>
          <Details>
            <Icon
              name="access-time"
              size={12}
              color={Colors.metal}
              // eslint-disable-next-line react-native/no-inline-styles
              style={{marginTop: 2, marginRight: 2}}
            />
            <EntryAt color={Colors.metal}>
              {moment(item.entryAt).calendar()}
            </EntryAt>

            {item.address && (
              <>
                <Icon
                  name="person-pin"
                  size={12}
                  color={Colors.metal}
                  // eslint-disable-next-line react-native/no-inline-styles
                  style={{marginTop: 2, marginRight: 2, marginLeft: 2}}
                />
                <Address>{item.address}</Address>
              </>
            )}
          </Details>
        </View>
        <ViewAmount>
          <Amount color={Colors.white}>
            <Currency value={item.amount} />
          </Amount>
        </ViewAmount>
      </Wrapper>
    </Button>
  );
}
