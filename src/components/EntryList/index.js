import React from 'react';
import {useNavigation} from '@react-navigation/native';
import styled from 'styled-components/native';
import Container from './../Container';
import EntryListItem from './EntryListItem';
import useEntries from '../../hooks/useEntries';

const List = styled.FlatList``;

export default function EntryList({days = 7, category = null}) {
  const navigation = useNavigation();
  const [entries] = useEntries(days, category);

  return (
    <Container
      title="Últimos lançamentos"
      label={`Últimos ${days} dias`}
      btText="Ver mais"
      onPress={() => navigation.navigate('Report')}>
      <List
        data={entries}
        renderItem={({item, index}) => (
          <EntryListItem
            item={item}
            isFirst={index === 0}
            isLast={index === entries.length - 1}
            onPress={() => navigation.navigate('NewEntry', {entry: item})}
          />
        )}
        keyExtractor={(item) => item.id}
      />
    </Container>
  );
}
