import React from 'react';
import NumberFormat from 'react-number-format';
import styled from 'styled-components/native';
const Text = styled.Text``;

export default function Currency({value}) {
  return (
    <NumberFormat
      value={parseFloat(value)}
      displayType={'text'}
      thousandSeparator={'.'}
      decimalSeparator={','}
      fixedDecimalScale={true}
      decimalScale={2}
      prefix={'R$ '}
      renderText={(item) => <Text>{item}</Text>}
    />
  );
}
