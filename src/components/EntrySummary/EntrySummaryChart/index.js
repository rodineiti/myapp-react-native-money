import React from 'react';
import styled from 'styled-components/native';
import {PieChart} from 'react-native-svg-charts';

const View = styled.View``;

export default function EntrySummaryChart({data = []}) {
  const pieData = data.map(({category, amount}) => ({
    key: category.id,
    value: amount,
    svg: {
      fill: category.color,
    },
    arc: {
      outerRadius: '100%',
      innerRadius: '80%',
    },
  }));

  return (
    <View>
      <PieChart
        // eslint-disable-next-line react-native/no-inline-styles
        style={{height: 100, width: 100, marginRight: 10}}
        data={pieData}
        svg={{
          fill: 'rgba(0,0,0,0.1)',
          stroke: 'rgba(0,0,0,0.1)',
          strokeWidth: 1,
        }}
        contentInset={{top: 0, bottom: 0}}
      />
    </View>
  );
}
