import React from 'react';
import {useNavigation} from '@react-navigation/native';
import Container from './../Container';
import EntrySummaryChart from './EntrySummaryChart';
import EntrySummaryList from './EntrySummaryList';
import styled from 'styled-components';
import useBalanceSumByCategory from '../../hooks/useBalanceSumByCategory';

const ViewSummary = styled.View`
  flex-direction: row;
  padding-right: 10px;
  padding-left: 10px;
`;

export default function EntrySummary({days = 7}) {
  const navigation = useNavigation();
  const [balanceSum] = useBalanceSumByCategory(days);
  return (
    <Container
      title={'Categorias'}
      label={`Últimos ${days} dias`}
      btText={'Ver mais'}
      onPress={() => navigation.navigate('Report')}>
      <ViewSummary>
        <EntrySummaryChart data={balanceSum} />
        <EntrySummaryList data={balanceSum} />
      </ViewSummary>
    </Container>
  );
}
