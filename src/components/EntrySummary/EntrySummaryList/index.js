import React from 'react';
import styled from 'styled-components/native';
import Svg, {Circle} from 'react-native-svg';
import Colors from '../../../styles/Colors';
import Currency from '../../Currency';

const ItemView = styled.View`
  flex-direction: row;
`;

const ItemText = styled.Text`
  font-size: 13px;
  color: ${(props) => props.color || '#000'};
  margin-top: 2px;
`;

const ItemValue = styled.Text`
  flex: 1;
  font-size: 13px;
  color: ${(props) => props.color || '#000'};
  margin-top: 2px;
  text-align: right;
`;

const List = styled.FlatList``;

export default function EntrySummaryList({data}) {
  return (
    <List
      data={data}
      renderItem={({item}) => (
        <ItemView>
          <Svg height="20" width="22">
            <Circle
              cx="10"
              cy="10"
              r="8"
              stroke={Colors.background}
              strokeWidth="0.5"
              fill={item.category.color || Colors.white}
            />
          </Svg>
          <ItemText color={Colors.white}>{item.category.name}</ItemText>
          <ItemValue color={Colors.white}>
            <Currency value={item.amount} />
          </ItemValue>
        </ItemView>
      )}
      keyExtractor={(item) => item.category.id}
    />
  );
}
