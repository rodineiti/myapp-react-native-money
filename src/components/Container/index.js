import React from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../styles/Colors';

const View = styled.View`
  background-color: ${(props) => props.bgcolor || '#000'};
  margin: 5px;
  border: 1px solid rgba(255, 255, 255, 0.2);
  border-radius: 5px;
  padding: 8px;
`;

const Title = styled.Text`
  font-size: 12px;
  font-weight: bold;
  margin-top: 10px;
  margin-bottom: 20px;
  color: ${(props) => props.color || '#fff'};
`;

const Wrapper = styled.View`
  flex-direction: row;
`;

const Text = styled.Text`
  flex: 1;
  font-size: 12px;
  color: ${(props) => props.color || '#fff'};
`;

const Button = styled.TouchableOpacity`
  flex-direction: row;
  align-items: center;
`;

const ButtonText = styled.Text`
  font-size: 12px;
  color: ${(props) => props.color || '#fff'};
  margin-left: 10px;
`;

export default function Container({children, title, label, btText, onPress}) {
  return (
    <View bgcolor={Colors.asphalt}>
      <Title color={Colors.white}>{title}</Title>
      {children}

      {(btText || onPress) && (
        <Wrapper>
          {label && <Text color={Colors.white}>{label}</Text>}

          {btText && (
            <Button onPress={onPress}>
              <Icon name="insert-chart" size={20} color={Colors.white} />
              <ButtonText olor={Colors.white}>{btText}</ButtonText>
            </Button>
          )}
        </Wrapper>
      )}
    </View>
  );
}
