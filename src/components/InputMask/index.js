import React, {useState} from 'react';
import {TextInputMask} from 'react-native-masked-text';
import styled from 'styled-components/native';
import Colors from '../../styles/Colors';

const View = styled.View`
  flex-direction: row;
  background-color: ${(props) => props.bgcolor || '#fff'};
  border-radius: 15px;
  margin: 20px 10px 20px 10px;
`;

const Input = styled(TextInputMask)`
  font-size: 28px;
  color: ${(props) => props.color || '#fff'};
  text-align: right;
  flex: 1;
  padding-left: 0;
  padding-right: 20px;
`;

const Button = styled.TouchableOpacity`
  flex-direction: row;
  padding: 20px;
`;

const ButtonText = styled.Text`
  font-size: 28px;
  color: ${(props) => props.color || '#fff'};
  min-width: 8px;
`;

export default function InputMask({
  type,
  value,
  onChange,
  onSetDebit,
  startWithDebit = true,
}) {
  const setDefaultDebit = () => {
    if (value === 0) {
      return startWithDebit ? -1 : 1;
    } else {
      return value <= 0 ? -1 : 1;
    }
  };
  const setDefaultPrefix = () => {
    if (value === 0) {
      return startWithDebit ? '-' : '';
    } else {
      return value <= 0 ? '-' : '';
    }
  };
  const [debit, setDebit] = useState(setDefaultDebit());
  const [prefix, setPrefix] = useState(setDefaultPrefix());
  const options = {
    precision: 2,
    separator: ',',
    delimiter: '.',
    unit: '',
    suffixunit: '',
  };

  return (
    <View bgcolor={Colors.asphalt}>
      <Button
        onPress={() => {
          if (debit < 0) {
            setDebit(1);
            setPrefix('');
            onSetDebit && onSetDebit(false);
          } else {
            setDebit(-1);
            setPrefix('-');
            onSetDebit && onSetDebit(true);
          }
          onChange(value * -1);
        }}>
        <ButtonText color={Colors.white}>{prefix}</ButtonText>
        <ButtonText color={Colors.white}>R$</ButtonText>
      </Button>
      <Input
        type={type}
        options={options}
        value={value}
        includeRawValueInChangeText={true}
        onChangeText={(maskedValue, rawValue) => {
          onChange(rawValue * debit);
        }}
        color={Colors.white}
      />
    </View>
  );
}
