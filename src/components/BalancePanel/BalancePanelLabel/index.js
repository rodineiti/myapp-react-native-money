import React from 'react';
import styled from 'styled-components/native';
import Colors from '../../../styles/Colors';

const View = styled.View`
  align-items: center;
  z-index: 1;
`;

const Label = styled.Text`
  font-size: ${(props) => props.sizeLabel}px;
  color: ${(props) => props.color || '#000'};
`;

const Value = styled.Text`
  font-size: ${(props) => props.sizeValue}px;
  color: ${(props) => props.color || '#000'};
`;

import Currency from './../../Currency';

export default function BalancePanelLabel({
  sizeLabel = 14,
  sizeValue = 36,
  currentBalance,
}) {
  return (
    <View>
      <Label color={Colors.white} sizeLabel={sizeLabel}>
        Saldo atual
      </Label>
      <Value color={Colors.white} sizeValue={sizeValue}>
        <Currency value={currentBalance} />
      </Value>
    </View>
  );
}
