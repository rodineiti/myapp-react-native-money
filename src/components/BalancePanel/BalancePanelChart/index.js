import React from 'react';
import styled from 'styled-components/native';
import {BarChart} from 'react-native-svg-charts';
import useBalanceSumByDate from '../../../hooks/useBalanceSumByDate';

const View = styled.View`
  padding-left: 20px;
  padding-right: 20px;
  margin-top: -20px;
  margin-bottom: 5px;
`;

export default function BalancePanelChart() {
  const [balanceSum] = useBalanceSumByDate();
  return (
    <View>
      <BarChart
        // eslint-disable-next-line react-native/no-inline-styles
        style={{height: 60}}
        data={balanceSum}
        svg={{
          fill: 'rgba(0,0,0,0.1)',
          stroke: 'rgba(0,0,0,0.1)',
          strokeWidth: 1,
        }}
        contentInset={{top: 0, bottom: 0}}
      />
    </View>
  );
}
