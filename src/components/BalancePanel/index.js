import React from 'react';
import {useNavigation} from '@react-navigation/native';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import LinearGradient from 'react-native-linear-gradient';
import BalancePanelChart from './BalancePanelChart';
import BalancePanelLabel from './BalancePanelLabel';
import Colors from './../../styles/Colors';
import useBalance from './../../hooks/useBalance';
import { StatusBar } from 'react-native';

const View = styled.View`
  margin-bottom: -20px;
`;

const Linear = styled(LinearGradient)``;

const Button = styled.TouchableOpacity`
  background-color: ${(props) => props.bgcolor || '#fff'};
  width: 50px;
  height: 50px;
  border-radius: 25px;
  margin-top: -25px;
  align-self: flex-end;
  align-items: center;
  justify-content: center;
  margin-right: 10px;
`;

export default function BalancePanel() {
  const navigation = useNavigation();
  const [balance] = useBalance();
  return (
    <View>
      <StatusBar barStyle="light-content" backgroundColor={Colors.violet} />
      <Linear colors={[Colors.violet, Colors.blue]}>
        <BalancePanelLabel currentBalance={balance} />
        <BalancePanelChart />
      </Linear>
      <Button
        bgcolor={Colors.green}
        // eslint-disable-next-line react-native/no-inline-styles
        style={{
          shadowColor: Colors.black,
          elevation: 5,
        }}
        title="Adicionar"
        onPress={() => navigation.navigate('NewEntry')}>
        <Icon name="add" size={30} color={Colors.white} />
      </Button>
    </View>
  );
}
