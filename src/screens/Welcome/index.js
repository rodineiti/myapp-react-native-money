import React, {useState} from 'react';
import styled from 'styled-components/native';
import useCategories from './../../hooks/useCategories';
import {saveEntry} from '../../services/Entries';
import {setConfig} from '../../services/Welcome';

import Colors from '../../styles/Colors';

const View = styled.View`
  flex: 1;
  background-color: ${(props) => props.bgcolor || '#fff'};
  padding: 10px;
`;

const Logo = styled.View`
  align-items: center;
  margin-top: 20px;
`;

const Img = styled.Image``;

const ViewButtons = styled.View`
  flex-direction: row;
  justify-content: center;
  padding-top: 5px;
  padding-bottom: 5px;
`;

const Button = styled.TouchableOpacity`
  border: 1px solid ${(props) => props.bdcolor || '#fff'};
  border-radius: 150px;
  padding: 10px 20px 10px 20px;
`;

const ButtonText = styled.Text`
  font-size: 18px;
  text-align: center;
  color: ${(props) => props.color || '#fff'};
`;

const Body = styled.View``;

const Title = styled.Text`
  color: ${(props) => props.color || '#000'};
  font-size: 28px;
  text-align: center;
  margin-top: 20px;
`;

const Description = styled.Text`
  color: ${(props) => props.color || '#000'};
  font-size: 18px;
  text-align: center;
  margin-top: 10px;
  margin-bottom: 20px;
`;

const Text = styled.Text`
  font-size: 28px;
  color: ${(props) => props.color || '#fff'};
  text-align: center;
`;

import LogoImage from './../../assets/logo-white.png';
import InputMask from './../../components/InputMask';

export default function Welcome({navigation}) {
  const [amount, setAmount] = useState(0);
  const [, , , initCategories] = useCategories();

  const onSubmit = async () => {
    saveEntry({
      amount,
      isInit: true,
      category: initCategories[0],
    });

    await setConfig();
    navigation.navigate('Main');
  };

  return (
    <View bgcolor={Colors.background}>
      <Logo>
        <Img source={LogoImage} />
      </Logo>
      <Body>
        <Title color={Colors.white}>Olá!</Title>
        <Description color={Colors.white}>
          Para começar o Smart Money, você precisa informar o saldo atual. Vamos
          lá?
        </Description>
      </Body>
      <Text>Informe seu saldo</Text>
      <InputMask
        type={'money'}
        value={amount}
        onChange={setAmount}
        startWithDebit={false}
      />
      <ViewButtons>
        <Button onPress={onSubmit} bdcolor={Colors.green}>
          <ButtonText color={Colors.green}>{`${'Continuar'}`}</ButtonText>
        </Button>
      </ViewButtons>
    </View>
  );
}
