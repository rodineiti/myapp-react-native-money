import React from 'react';
import styled from 'styled-components/native';
import BalancePanel from '../../components/BalancePanel';
import EntryList from '../../components/EntryList';
import EntrySummary from '../../components/EntrySummary';
import Colors from '../../styles/Colors';

const View = styled.View`
  flex: 1;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

const Scroll = styled.ScrollView``;

export default function Main() {
  return (
    <View bgcolor={Colors.background}>
      <BalancePanel />
      <Scroll>
        <EntrySummary />
        <EntryList />
      </Scroll>
    </View>
  );
}
