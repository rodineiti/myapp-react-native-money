import React, {useEffect} from 'react';
import {StatusBar} from 'react-native';
import styled from 'styled-components/native';
import {getConfig} from '../../services/Welcome';
import Colors from '../../styles/Colors';

const View = styled.View`
  flex: 1;
  background-color: ${(props) => props.bgcolor || '#fff'};
  justify-content: center;
  align-items: center;
`;

const Logo = styled.View`
  align-items: center;
  margin-top: 20px;
`;

const ActiveIndicator = styled.ActivityIndicator`
  margin-top: 20px;
`;

const Img = styled.Image``;

import LogoImage from './../../assets/logo-white.png';

export default function Preload({navigation}) {
  useEffect(() => {
    const check = async () => {
      (await getConfig())
        ? navigation.navigate('Main')
        : navigation.navigate('Welcome');
    };
    check();
  }, [navigation]);

  return (
    <View bgcolor={Colors.background}>
      <StatusBar barStyle="light-content" backgroundColor={Colors.background} />
      <Logo>
        <Img source={LogoImage} />
      </Logo>
      <ActiveIndicator color={Colors.violet} size={60} />
    </View>
  );
}
