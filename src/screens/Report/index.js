import React, {useState, useEffect} from 'react';
import styled from 'styled-components/native';
import {Picker} from '@react-native-picker/picker';
import BalanceLabel from '../../components/BalanceLabel';
import EntrySummary from '../../components/EntrySummary';
import EntryList from '../../components/EntryList';
import Colors from '../../styles/Colors';
import {getAllCategories} from '../../services/Categories';
import { StatusBar } from 'react-native';

const View = styled.View`
  flex: 1;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

const FilterView = styled.View`
  width: 98%;
  align-self: center;
`;

const ViewButtons = styled.View`
  flex-direction: row;
  justify-content: center;
  padding-top: 5px;
  padding-bottom: 5px;
`;

const Button = styled.TouchableOpacity`
  border: 1px solid ${(props) => props.bdcolor || '#fff'};
  border-radius: 150px;
  padding: 10px 20px 10px 20px;
`;

const ButtonText = styled.Text`
  font-size: 18px;
  text-align: center;
  color: ${(props) => props.color || '#fff'};
`;

const Scroll = styled.ScrollView``;

const ViewSelect = styled.View`
  border: 1px solid ${(props) => props.bdcolor || '#fff'};
  border-radius: 150px;
  margin-bottom: 5px;
  padding-left: 10px;
`;

export default function Report({navigation}) {
  const relativeDays = [1, 3, 7, 15, 21, 30, 45, 60, 90, 180, 365];
  const [categories, setCategories] = useState([]);
  const [category, setCategory] = useState(null);
  const [days, setDays] = useState(7);
  useEffect(() => {
    const loadCategorias = async () => {
      const data = await getAllCategories();
      setCategories(data);
    };
    loadCategorias();
  }, []);

  return (
    <View bgcolor={Colors.background}>
      <StatusBar barStyle="light-content" backgroundColor={Colors.background} />
      <BalanceLabel currentBalance={2.064} sizeLabel={12} sizeValue={18} />

      <FilterView>
        <ViewSelect bdcolor={Colors.champagneDark}>
          <Picker
            style={{color: Colors.white}}
            selectedValue={category}
            onValueChange={(itemValue, itemIndex) => setCategory(itemValue)}>
            <Picker.Item label="Todas as categorias" value="" />
            {categories.length > 0 &&
              categories.map((item) => (
                <Picker.Item key={item.id} label={item.name} value={item} />
              ))}
          </Picker>
        </ViewSelect>
        <ViewSelect bdcolor={Colors.champagneDark}>
          <Picker
            selectedValue={days}
            style={{color: Colors.white}}
            onValueChange={(itemValue, itemIndex) => setDays(itemValue)}>
            {relativeDays.map((item, key) => (
              <Picker.Item
                key={key}
                label={`Últimos ${item} dia(s)`}
                value={item}
              />
            ))}
          </Picker>
        </ViewSelect>
      </FilterView>

      <Scroll horizontal={false}>
        <EntrySummary days={days} />
        <EntryList days={days} category={category} />
      </Scroll>

      <ViewButtons>
        <Button onPress={() => navigation.goBack()} bdcolor={Colors.green}>
          <ButtonText color={Colors.green}>{`${'Fechar'}`}</ButtonText>
        </Button>
      </ViewButtons>
    </View>
  );
}
