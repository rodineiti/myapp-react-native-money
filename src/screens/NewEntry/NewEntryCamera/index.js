import React, {useState} from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Colors from '../../../styles/Colors';

const View = styled.View``;

const Modal = styled.Modal``;

const Button = styled.TouchableOpacity`
  background-color: ${(props) => props.bgcolor || '#fff'};
  width: 59px;
  height: 59px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  margin-left: 10px;
`;

export default function NewEntryCamera({photo, onSetPhoto}) {
  const [show, setShow] = useState(false);
  return (
    <View>
      <Button bgcolor={Colors.asphalt} onPress={() => setShow(true)}>
        <Icon name="photo-camera" size={30} color={Colors.white} />
      </Button>
      <Modal animationType="slide" transparent={false} visible={show} />
    </View>
  );
}
