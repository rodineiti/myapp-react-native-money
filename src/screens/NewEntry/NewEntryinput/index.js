import React, {useState, useEffect} from 'react';
import styled from 'styled-components/native';
import {
  getCreditCategories,
  getDebitCategories,
} from '../../../services/Categories';
import Colors from '../../../styles/Colors';

const View = styled.View``;

const Modal = styled.Modal``;

const Button = styled.TouchableOpacity`
  border-radius: 15px;
  margin: 10px 10px 10px 10px;
  padding: 20px;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

const Text = styled.Text`
  font-size: 28px;
  color: ${(props) => props.color || '#000'};
  text-align: center;
`;

const Close = styled.TouchableOpacity`
  border-radius: 150px;
  margin: 10px 10px 10px 10px;
  padding: 10px;
  border: 1px solid ${(props) => props.bdcolor || '#fff'};
  text-align: center;
  align-self: center;
`;

const TextClose = styled.Text`
  font-size: 22px;
  color: ${(props) => props.color || '#000'};
  text-align: center;
`;

const ModalBody = styled.View`
  flex: 1;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

const List = styled.FlatList``;

const Item = styled.TouchableOpacity`
  border-radius: 15px;
  margin: 10px 10px 10px 10px;
  padding: 20px;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

export default function NewEntryInput({setCategory, debit, category}) {
  const [show, setShow] = useState(false);
  const [categories, setCategories] = useState([]);
  useEffect(() => {
    const loadCategorias = async () => {
      let data;
      if (debit <= 0) {
        data = await getDebitCategories();
      } else {
        data = await getCreditCategories();
      }
      setCategories(data);
    };
    loadCategorias();
  }, [debit]);

  return (
    <View>
      <Button bgcolor={Colors.asphalt} onPress={() => setShow(true)}>
        <Text color={Colors.white}>{category.name}</Text>
      </Button>
      {show && (
        <Modal visible={show} transparent={true} animationType="slide">
          <ModalBody bgcolor={Colors.background}>
            <List
              data={categories}
              renderItem={({item, index}) => (
                <Item
                  bgcolor={Colors.asphalt}
                  onPress={() => {
                    setCategory(item);
                    setShow(false);
                  }}>
                  <TextClose color={item.color}>{item.name}</TextClose>
                </Item>
              )}
              keyExtractor={(item) => item.id}
            />
            <Close bdcolor={Colors.green} onPress={() => setShow(false)}>
              <TextClose color={Colors.green}>Fechar</TextClose>
            </Close>
          </ModalBody>
        </Modal>
      )}
    </View>
  );
}
