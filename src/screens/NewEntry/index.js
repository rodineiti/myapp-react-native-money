import React, {useState} from 'react';
import {Alert, StatusBar} from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import Geocoder from 'react-native-geocoding';
import {useNavigation, useRoute} from '@react-navigation/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import styled from 'styled-components/native';
import BalanceLabel from '../../components/BalanceLabel';
import InputMask from '../../components/InputMask';
import NewEntryInput from './NewEntryinput';
import NewEntryInputDate from './NewEntryinputDate';
import useEntries from './../../hooks/useEntries';
import Colors from '../../styles/Colors';

const APIKEY = '';

const View = styled.View`
  flex: 1;
  padding: 10px;
  background-color: ${(props) => props.bgcolor || '#fff'};
`;

const ViewForm = styled.View`
  flex: 1;
  padding-left: 10px;
  padding-right: 10px;
`;

const Input = styled.TextInput`
  border-radius: 15px;
  margin: 10px 10px 10px 10px;
  padding: 25px;
  background-color: ${(props) => props.bgcolor || '#fff'};
  text-align: left;
  font-size: 20px;
  color: ${(props) => props.color || '#fff'};
`;

const ViewButtons = styled.View`
  background-color: ${(props) => props.bgcolor || '#fff'};
  padding-top: 5px;
  padding-bottom: 5px;
  flex-direction: row;
  justify-content: center;
`;

const Button = styled.TouchableOpacity`
  border: 1px solid ${(props) => props.bdcolor || '#fff'};
  border-radius: 150px;
  padding: 10px 20px 10px 20px;
`;

const ButtonText = styled.Text`
  font-size: 18px;
  text-align: center;
  color: ${(props) => props.color || '#fff'};
`;

const ButtonIcon = styled.TouchableOpacity`
  background-color: ${(props) => props.bgcolor || '#fff'};
  width: 59px;
  height: 59px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
  margin-left: 10px;
`;

const ViewButtonsIcon = styled.View`
  flex-direction: row;
  justify-content: center;
  margin-right: 10px;
  margin-left: 10px;
`;

export default function NewEntry() {
  const navigation = useNavigation();
  const route = useRoute();
  const entry = route.params?.entry ?? {
    id: null,
    amount: '0.00',
    description: '',
    entryAt: new Date(),
    category: {id: null, name: 'Selecione'},
    latitude: '',
    longitude: '',
    address: '',
    photo: '',
  };

  const [amount, setAmount] = useState(entry.amount);
  const [debit, setDebit] = useState(entry.amount <= 0);
  const [description, setDescription] = useState(entry.description);
  const [category, setCategory] = useState(entry.category);
  const [entryAt, setEntryAt] = useState(entry.entryAt);
  const [latitude, setLatitude] = useState(entry.latitude);
  const [longitude, setLongitude] = useState(entry.longitude);
  const [address, setAddress] = useState(entry.address);
  const [photo, setPhoto] = useState(entry.photo);

  const [, saveEntry, removeEntry] = useEntries();

  const isValid = () => {
    if (parseFloat(amount) !== 0) {
      return true;
    }
    return false;
  };

  const save = () => {
    const data = {
      amount: parseFloat(amount),
      description,
      category,
      entryAt,
      latitude,
      longitude,
      address,
      photo,
    };
    saveEntry(data, entry);

    goBack();
  };

  const destroy = () => {
    if (entry) {
      removeEntry(entry);
    }
    goBack();
  };

  const goBack = () => {
    navigation.goBack();
  };

  const onDelete = () => {
    Alert.alert(
      'Apagar?',
      'Você tem certeza que deseja deletar este lançamento?',
      [
        {text: 'Não', style: 'cancel'},
        {
          text: 'Sim',
          onPress: () => {
            destroy();
          },
        },
      ],
      {cancelable: false},
    );
  };

  const getLocation = (lat, lng) => {
    Geocoder.init(APIKEY);
    if (lat && lng) {
      Geocoder.from(lat, lng)
        .then((json) => {
          const formatted_address = json.results[0].formatted_address;
          Alert.alert('Localização', formatted_address, [
            {
              text: 'Cancelar',
              onPress: () => {
                setLatitude('');
                setLongitude('');
                setAddress('');
              },
              style: 'cancel',
            },
            {
              text: 'Confirmar',
              onPress: () => {
                setLatitude(lat);
                setLongitude(lng);
                setAddress(formatted_address);
              },
            },
          ]);
        })
        .catch((error) => {
          console.log('ERRO', error);
          Alert.alert(
            'Erro ao recuperar localização',
            'Erro ao recuperar localização, verifique as permissões',
          );
        });
    } else {
      Alert.alert(
        'Erro ao recuperar localização',
        'Latitude e longitude não definidos',
      );
    }
  };

  const getCurrentPosition = () => {
    if (address) {
      Alert.alert('Localização', address, [
        {
          text: 'Apagar',
          onPress: () => {
            setLatitude('');
            setLongitude('');
            setAddress('');
          },
          style: 'cancel',
        },
        {
          text: 'OK',
          onPress: () => console.log('Onpress Ok'),
        },
      ]);
    } else {
      Geolocation.getCurrentPosition(
        (info) => {
          const {coords} = info;
          getLocation(coords.latitude, coords.longitude);
        },
        (error) => {
          Alert.alert('Erro ao recuperar Localização', error);
        },
      );
    }
  };

  return (
    <View bgcolor={Colors.background}>
      <StatusBar barStyle="light-content" backgroundColor={Colors.background} />
      <BalanceLabel currentBalance={2.064} sizeLabel={12} sizeValue={18} />

      <ViewForm>
        <InputMask
          type={'money'}
          value={amount}
          onChange={setAmount}
          onSetDebit={setDebit}
        />
        <Input
          placeholder="Descrição"
          placeholderTextColor={Colors.white}
          bgcolor={Colors.asphalt}
          color={Colors.white}
          value={description}
          onChangeText={(t) => setDescription(t)}
        />
        <NewEntryInput
          setCategory={setCategory}
          debit={debit}
          category={category}
        />
        <ViewButtonsIcon>
          <NewEntryInputDate value={entryAt} onSetEntryAt={setEntryAt} />
          <ButtonIcon
            bgcolor={address ? Colors.blue : Colors.asphalt}
            onPress={getCurrentPosition}>
            <Icon name="gps-fixed" size={30} color={Colors.white} />
          </ButtonIcon>
          {entry.id && (
            <ButtonIcon bgcolor={Colors.red} onPress={onDelete}>
              <Icon name="delete" size={30} color={Colors.white} />
            </ButtonIcon>
          )}
        </ViewButtonsIcon>
      </ViewForm>

      <ViewButtons bgcolor={Colors.background}>
        <Button
          onPress={() => {
            isValid() && save();
          }}
          bdcolor={Colors.green}>
          <ButtonText color={Colors.green}>{`${
            entry.id ? 'Atualizar' : 'Salvar'
          }`}</ButtonText>
        </Button>
        <Button bdcolor={Colors.background} onPress={goBack}>
          <ButtonText color={Colors.white}>Cancelar</ButtonText>
        </Button>
      </ViewButtons>
    </View>
  );
}
