import React, {useState} from 'react';
import styled from 'styled-components/native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import Colors from '../../../styles/Colors';

const View = styled.View``;

const Button = styled.TouchableOpacity`
  background-color: ${(props) => props.bgcolor || '#fff'};
  width: 59px;
  height: 59px;
  border-radius: 30px;
  align-items: center;
  justify-content: center;
`;

export default function NewEntryInputDate({value, onSetEntryAt}) {
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };

  const handleConfirm = (date) => {
    console.warn('A date has been picked: ', date);
    onSetEntryAt(date);
    hideDatePicker();
  };

  return (
    <View>
      <Button bgcolor={Colors.asphalt} onPress={showDatePicker}>
        <Icon name="today" size={30} color={Colors.white} />
      </Button>
      {isDatePickerVisible && (
        <DateTimePickerModal
          isVisible={isDatePickerVisible}
          mode="date"
          datePickerModeAndroid="calendar"
          titleIOS="Data de vencimento"
          cancelTextIOS="Cancelar"
          confirmTextIOS="OK"
          date={value}
          onConfirm={handleConfirm}
          onCancel={hideDatePicker}
        />
      )}
    </View>
  );
}
